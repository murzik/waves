This repository (and it's [web site](https://murzik.gitlab.io/waves/)) 
containes music I've created in my spare time. All tracks are licensed under
[CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/).